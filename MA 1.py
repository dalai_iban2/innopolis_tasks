import asyncio
import datetime
import requests
import re
import aiohttp

openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'

cities = []
with open('cities.txt') as cities_file:
    file_content = cities_file.readlines()
    for line in file_content:
        city = re.findall(r'[A-z]+', line)
        if city:
            cities.append(city[0] if len(city) == 1 else city[0]+' '+city[1])


class Weather:
    def __init__(self, city_name, city_temp, city_pressure, city_humidity, city_wind, city_sunrise, city_sunset, day_len):
        self.city_name = city_name
        self.city_temp = city_temp
        self.city_pressure = city_pressure
        self. city_humidity = city_humidity
        self.city_wind = city_wind
        self.city_sunrise = city_sunrise
        self.city_sunset = city_sunset
        self.day_len = day_len

        
def weather_pars(r_text):
    city_name = re.search(r'name":"[a-zA-z -]+', r_text).group(0).split(":")[1].replace('"', "")
    city_temp = re.search(r'temp":-\d+.\d', r_text).group(0).split(":")[1] if re.search(r'temp":-\d+.\d', r_text) != None else re.search(r'temp":\d+.\d', r_text).group(0).split(":")[1]
    city_wind = re.search(r'speed":\d+.\d', r_text).group(0).split(":")[1] if re.search(r'speed":\d+.\d', r_text) != None else re.search(r'speed":\d+', r_text).group(0).split(":")[1]
    city_humidity = re.search(r'humidity":\d+', r_text).group(0).split(":")[1]
    pressure = int(re.search(r'pressure":\d+', r_text).group(0).split(":")[1])*0.7500616827041699
    city_pressure = f"{pressure: .1f}"
    sunrise = int(re.search(r'sunrise":\d+', r_text).group(0).split(":")[1])
    sunset = int(re.search(r'sunset":\d+', r_text).group(0).split(":")[1])
    day_len = str(datetime.datetime.fromtimestamp(sunset-sunrise))[-8:]
    timedelta = int(re.search(r'timezone":\d+', r_text).group(0).split(":")[1])
    timezone = int(str(datetime.datetime.fromtimestamp(timedelta))[-8:-6])-6
    city_sunrise = str(int(str(datetime.datetime.fromtimestamp(sunrise))[-8:-6])+timezone)+str(datetime.datetime.fromtimestamp(sunrise))[-6:]
    city_sunset = str(int(str(datetime.datetime.fromtimestamp(sunset))[-8:-6])+timezone)+str(datetime.datetime.fromtimestamp(sunset))[-6:]
    current_city_weather = Weather(city_name, city_temp, city_pressure, city_humidity, city_wind, city_sunrise, city_sunset, day_len)
    return current_city_weather
    
    
async def get_weather(city_name):
    async with aiohttp.ClientSession() as session:
        url = f'https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={openweatherAPI}&units=metric'
        async with session.get(url=url) as response:
            if response.status == 200:
                current_city_weather = weather_pars(await response.text())
                print("-"*40)
                print(f'Город:\t{current_city_weather.city_name}\nТемпература: {current_city_weather.city_temp} °C\nВлажность: {current_city_weather.city_humidity} %\nДавление: {current_city_weather.city_pressure} мм.рт.ст.\nВетер: {current_city_weather.city_wind} м/с'
                      f'\nВосход: {current_city_weather.city_sunrise}\nЗакат: {current_city_weather.city_sunset}\nДлительность дня:  {current_city_weather.day_len}')                
            
            
async def main():
    tasks = []
    for name in cities:
        task = asyncio.create_task(get_weather(name))
        tasks.append(task)
    await asyncio.gather(*tasks)
    
    
loop = asyncio.get_event_loop()
loop.run_until_complete(main())